<?php get_header(); ?>

<section class="container content">
  <div class="row">
    <!-- Conditional untuk page menggunakan page slug -->
    <?php if(is_page('contact')): // contact = page slug ?>
      <div class="col-sm-8 col-sm-offset-2">
        <?php while(have_posts()): the_post();
          the_content();
        endwhile; ?>
      </div>
    <?php  else: ?>
      <?php while(have_posts()): the_post();
        the_content();
      endwhile; ?>
    <?php endif; ?>
  </div>
</section>










From page.php
<?php get_footer(); ?>