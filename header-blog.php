<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <?php wp_head(); // load the css/js files ?>
</head>
<body <?php body_class(); ?> >

  <!-- Get featured image from the blog posts page -->
  <?php $page_blog = get_option('page_for_posts');
  $id_image = get_post_thumbnail_id($page_blog);
  $featured = wp_get_attachment_image_src($id_image, 'full');
  // Get the featured image -->
  $featured = $featured[0]; ?>
  
  <header class="site-header" style="background-image:url(<?php echo $featured ?>);">
    <nav class="main-navigation">
      <div class="container">
        <div class="row">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
              <span class="sr-only">Toggle Navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Add a logo image and a link to the home page  -->
            <a href="<?php echo esc_url(home_url('/')); ?>">
              <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="Logo">
            </a>
          </div>
          <div class="navbar-right">
            <!-- Print the menu -->
            <?php wp_nav_menu(array(
              'theme_location' => 'main_menu',
              'container_id' => 'navbar',
              'container_class' => 'collapse navbar-collapse',
              'menu_class' => 'nav navbar-nav navbar-right'
            )); ?> 
          </div>
        </div>
        <!-- End row -->
      </div>
      <!-- container -->
    </nav>
    <!-- Main navigation -->

    <div class="container">
		  <div class="row">
				<div class="col-md-7">
					<div class="page-title">
            <!-- Get data from the blog posts page -->
            <?php
              $blog_page = get_option('page_for_posts');
              $title = get_the_title($blog_page);
            ?>
              <!-- Print the Blog page title -->
              <h1 class="title"><span><?php echo $title; ?></span></h1>

					</div>
				</div>
			</div>

    </div>
  </header>
