<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <?php wp_head(); // load the css/js files ?>
</head>
<body <?php body_class(); ?> >

  <!-- Print featured image as a background -->
  <?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
  <!-- Get the featured image -->
  <?php $featured = $featured[0]; ?>
  
  <header class="site-header" style="background-image:url(<?php echo $featured ?>);">
    <nav class="main-navigation">
      <div class="container">
        <div class="row">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
              <span class="sr-only">Toggle Navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- Add a logo image and a link to the home page  -->
            <a href="<?php echo esc_url(home_url('/')); ?>">
              <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="Logo">
            </a>
            
          </div>
          <div class="navbar-right">
            <!-- Print the menu -->
            <?php wp_nav_menu(array(
              'theme_location' => 'main_menu',
              'container_id' => 'navbar',
              'container_class' => 'collapse navbar-collapse',
              'menu_class' => 'nav navbar-nav navbar-right'
            )); ?> 
          </div>
        </div>
        <!-- End row -->
      </div>
      <!-- container -->
    </nav>
    <!-- Main navigation -->

    <div class="container">
		  <div class="row">
				<div class="col-md-7">
					<div class="page-title">
            <!-- Jika front-page.php -->
            <!-- Print the blog description -->
            <?php if(is_front_page()): ?>
              <?php $description = get_bloginfo('description', 'display'); ?>
              <h1 class="title"><span><?php echo $description; ?></span></h1>
            <?php elseif (is_category()): ?>
              <!-- Jika category.php -->
              <!-- Print the category -->
              <!-- <h1 class="title"><span><?php // the_archive_title(); ?></span></h1> -->
              <!-- Tanpa text category: -->
              <h1 class="title"><span><?php single_cat_title(); ?></span></h1>
            <?php else: ?>
              <!-- Print the page title -->
              <h1 class="title"><span><?php the_title(); ?></span></h1>
            <?php endif; ?>
					</div>
				</div>
			</div>

    </div>
  </header>
