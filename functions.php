<?php
// Add css dan js files
function ttb_styles() {
  wp_register_style('normalize', get_template_directory_uri(). '/css/normalize.css', array(), '6.0.0');
  wp_enqueue_style('normalize');

  wp_register_style('fontAwesome', get_template_directory_uri(). '/css/font-awesome.min.css', array(), '4.6.3');
  wp_enqueue_style('fontAwesome');

  wp_register_style('bootstrapcss', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", array(), '3.3.7');
  wp_enqueue_style('bootstrapcss');

  wp_register_style('ralewayFont', "https://fonts.googleapis.com/css?family=Raleway:900", array(), '1.0.0');
  wp_enqueue_style('ralewayFont');

  wp_register_style('libreBakersvilleFont', "https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700", array(), '1.0.0');
  wp_enqueue_style('libreBakersvilleFont');


  wp_register_style('style', get_template_directory_uri(). '/style.css', array(), '1.0');
  wp_enqueue_style('style');

  wp_enqueue_script('jquery');

  wp_register_script('bootstrapjs', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", array('jquery'), '3.3.7', true ); // true = load the file in the footer
  wp_enqueue_script('bootstrapjs');

}
add_action('wp_enqueue_scripts', 'ttb_styles');


// Add menus
register_nav_menus(array(
  'main_menu' => __('Main Menu', 'thetravelblog'),
  'social_menu' => __('Social Menu', 'thetravelblog')
));

add_theme_support('post-thumbnails');

add_image_size('entry', 750, 490, true); // true = crop the image


// Register widget for footer and sidebar
function ttb_widgets() {
  // For footer
  register_sidebar(array(
    'name' => __('Footer Widget'),
    'id' => 'footer_widget',
    'description' => 'Widgets for the footer',
    'before_widget' => '<div id="%1$s" class="widget col-sm-6 %2$s">',
    'after_widget' =>'</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>'
  ));

  // For sidebar
  register_sidebar(array(
    'name' => __('Sidebar Widget'),
    'id' => 'sidebar_widget',
    'description' => 'Widgets for the sidebar',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' =>'</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>'
  ));
}
add_action('widgets_init', 'ttb_widgets');







?>